package com.people_mvvm_kotlin.folder

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.people_mvvm_kotlin.R
import com.people_mvvm_kotlin.databinding.ItemPeopleBinding
import com.people_mvvm_kotlin.model.ItemPeopleModel
import com.people_mvvm_kotlin.model.People


class PeopleAdapter : RecyclerView.Adapter<PeopleAdapter.PeopleAdapterViewHolder> {
    var peopleList: List<People> = ArrayList()

    constructor() : super()

    override fun getItemCount(): Int {
        return peopleList.size
    }

    override fun onBindViewHolder(holder: PeopleAdapterViewHolder?, position: Int) {
        holder!!.bindPeople(peopleList.get(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PeopleAdapterViewHolder {
        val itemPeopleBinding = DataBindingUtil.inflate<ItemPeopleBinding>(LayoutInflater.from(parent!!.context), R.layout.item_people,parent,false)
        return PeopleAdapterViewHolder(itemPeopleBinding)
    }


    class PeopleAdapterViewHolder : RecyclerView.ViewHolder {
        var itemPeopleBinding: ItemPeopleBinding? = null

        constructor(itemView: ItemPeopleBinding?) : super(itemView?.root){
            this.itemPeopleBinding = itemView
        }

         fun bindPeople(people:People){
           if(itemPeopleBinding?.itemPeopleViewModel==null){
               itemPeopleBinding?.itemPeopleViewModel = ItemPeopleModel(people,itemView.context)
           }else{
               itemPeopleBinding?.itemPeopleViewModel?.setPeople(people)
           }
        }
    }
}
