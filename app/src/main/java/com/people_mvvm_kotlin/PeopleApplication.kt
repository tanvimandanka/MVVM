package com.people_mvvm_kotlin

import android.app.Application
import android.content.Context
import com.people_mvvm_kotlin.data.PeopleFactory
import com.people_mvvm_kotlin.model.PeopleService



class PeopleApplication : Application() {
    private var peopleService: PeopleService? = null

    companion object {
        fun create(context: Context): PeopleApplication {
            return get(context)
        }

        fun get(context: Context): PeopleApplication {
            return context.applicationContext as PeopleApplication
        }
    }

    fun getPeopleService() : PeopleService{
        if (peopleService == null){
            peopleService = PeopleFactory.create()
        }
        return peopleService as PeopleService
    }


    override fun onCreate() {
        super.onCreate()
    }
}
