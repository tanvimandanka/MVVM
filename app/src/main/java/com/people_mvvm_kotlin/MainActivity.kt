package com.tanvi.people_mvvm_kotlin

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.people_mvvm_kotlin.R
import com.people_mvvm_kotlin.databinding.ActivityMainBinding
import com.people_mvvm_kotlin.folder.PeopleAdapter
import com.people_mvvm_kotlin.model.PeopleViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(),Observer {
    lateinit var mActivityMainBinding: ActivityMainBinding
    lateinit var mPeopleAdapter: PeopleAdapter
    lateinit var mPeopleViewModel: PeopleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(toolbar)
        mPeopleViewModel = PeopleViewModel(this)
        mActivityMainBinding.model = mPeopleViewModel
        mPeopleViewModel.addObserver(this)
        setAdapter()
    }

    private fun setAdapter() {
        mPeopleAdapter = PeopleAdapter()
        mActivityMainBinding.listPeople.adapter = mPeopleAdapter
        mActivityMainBinding.listPeople.layoutManager = LinearLayoutManager(this)
    }

    override fun update(observable: Observable?, p1: Any?) {
            if (observable is PeopleViewModel){
                val peopleViewModel = observable
                mPeopleAdapter.peopleList = peopleViewModel.peopleList
                mPeopleAdapter.notifyDataSetChanged()
            }
    }
}
