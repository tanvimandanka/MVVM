package com.people_mvvm_kotlin.data

import com.people_mvvm_kotlin.model.People
import com.google.gson.annotations.SerializedName

/**
 * Created by tanvi on 27/12/17.
 */
class PeopleResponse {
 @SerializedName("results")
 var peopleList : List<People> = ArrayList()
}