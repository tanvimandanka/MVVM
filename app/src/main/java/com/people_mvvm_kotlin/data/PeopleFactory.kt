package com.people_mvvm_kotlin.data

import com.people_mvvm_kotlin.model.PeopleService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class PeopleFactory{
    companion object {
        val BASE_URL = "http://api.randomuser.me/"
        val RANDOM_USER_URL = "http://api.randomuser.me/?results=10&nat=en"
        val PROJECT_URL = "https://github.com/erikcaffrey/People-MVVM"
        fun create() : PeopleService{
            val retrofit:Retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return retrofit.create(PeopleService::class.java)
        }
    }
}
