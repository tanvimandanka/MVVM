package com.people_mvvm_kotlin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tanvi.people_mvvm_kotlin.MainActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn_login.setOnClickListener {
            if (validate()) {
                jumpToMainActivity()
            }
        }
    }

    private fun jumpToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun validate(): Boolean {
        val email = input_email.text.toString().trim()
        val password = input_password.text.toString().trim()

        if (email.isBlank()) {
            input_email.setError("Please enter your email!")
            return false
        }

        if (password.isBlank()) {
            input_password.setError("Please enter your password!")
            return false
        }
        return true
    }
}