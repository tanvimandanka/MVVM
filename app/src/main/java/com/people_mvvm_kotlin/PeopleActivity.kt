package com.people_mvvm_kotlin

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.people_mvvm_kotlin.databinding.PeopleDetailActivityBinding

import com.people_mvvm_kotlin.model.People
import com.people_mvvm_kotlin.model.PeopleDetailViewModel


class PeopleActivity : AppCompatActivity() {

    private var mPeopleDetailActivityBinding: PeopleDetailActivityBinding? = null
    private var mPeopleDetailViewModel: PeopleDetailViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPeopleDetailActivityBinding = DataBindingUtil.setContentView(this, R.layout.people_detail_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        getIntentExtra()
    }

    private fun getIntentExtra() {
        val people = intent.getSerializableExtra(PUT_EXTRA) as People
        mPeopleDetailViewModel = PeopleDetailViewModel(this, people = people)
        mPeopleDetailActivityBinding?.mPeopleDetailViewModel = mPeopleDetailViewModel
    }

    companion object {
        val PUT_EXTRA = "Extra"
        fun launchPeopleActivity(context: Context, people: People?): Intent {
            val intent = Intent(context, PeopleActivity::class.java)
            intent.putExtra(PUT_EXTRA, people)
            return intent
        }
    }

}
