package com.people_mvvm_kotlin.model


import com.people_mvvm_kotlin.data.PeopleResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url


interface PeopleService {
  @GET
  fun fetchPeople(@Url url:String): Observable<PeopleResponse>
}

