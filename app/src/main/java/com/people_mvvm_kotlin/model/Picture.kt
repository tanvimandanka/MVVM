/**
 * Copyright 2016 Erik Jhordan Rey.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.people_mvvm_kotlin.model

import com.google.gson.annotations.SerializedName

import java.io.Serializable

class Picture : Serializable {

    @SerializedName("large")
    var large: String? = null

    @SerializedName("medium")
    var medium: String? = null

    @SerializedName("thumbnail")
    var thumbnail: String? = null
}
