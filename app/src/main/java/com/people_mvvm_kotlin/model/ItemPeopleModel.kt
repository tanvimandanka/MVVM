package com.people_mvvm_kotlin.model

import android.content.Context
import android.databinding.BaseObservable
import android.view.View
import com.people_mvvm_kotlin.PeopleActivity


class ItemPeopleModel : BaseObservable {
    private var people: People? = null
    private var context: Context? = null

    constructor(people: People, context: Context) : super() {
        this.people = people
        this.context = context
    }

    fun setPeople(people: People) {
        this.people = people
    }

    fun getLabelName() : String? {
        return people?.name?.last?.trim()
    }

    fun getPhone() : String?{
        return people?.phone?.trim()
    }

    fun getEmail() : String?{
        return people?.mail?.trim()
    }

    fun getPictureProfile(): String? {
        return people?.picture?.medium
    }

    fun onItemClicked(view: View){
        context?.startActivity(PeopleActivity.Companion.launchPeopleActivity(view.context,people))
    }

}
