package com.people_mvvm_kotlin.model

import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.people_mvvm_kotlin.PeopleApplication
import com.people_mvvm_kotlin.data.PeopleFactory
import com.people_mvvm_kotlin.data.PeopleResponse
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String) {
    Glide.with(imageView.context).load(url).into(imageView)
}
class PeopleViewModel : Observable {
    var peopleProgress: ObservableInt
    var peopleLabelMessage: ObservableField<String>
    var peopleLabel: ObservableInt
    var peopleRecyclerView: ObservableInt
    var context: Context
    var compositeDesposable: CompositeDisposable = CompositeDisposable();
    var peopleList: List<People> = ArrayList<People>()

    constructor(context: Context) {
        peopleProgress = ObservableInt(View.GONE)
        peopleLabelMessage = ObservableField("Click on Button To add people!")
        peopleLabel = ObservableInt(View.VISIBLE)
        peopleRecyclerView = ObservableInt(View.GONE)
        this.context = context
    }

    fun onFabButtonClicked(view: View) {
        val peopleApplication = PeopleApplication.create(context)
        val peopleService = peopleApplication.getPeopleService()

        peopleProgress.set(View.VISIBLE)
        peopleLabel.set(View.GONE)

        peopleService.fetchPeople(PeopleFactory.RANDOM_USER_URL)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<PeopleResponse> {
                    override fun onComplete() {

                    }

                    override fun onNext(peoples: PeopleResponse) {
                        peopleProgress.set(View.GONE)
                        peopleRecyclerView.set(View.VISIBLE)
                        changePeopleDataSet(peoples.peopleList)
                    }

                    override fun onError(e: Throwable?) {
                        peopleProgress.set(View.GONE)
                        peopleLabel.set(View.VISIBLE)
                        peopleLabelMessage.set("Error while loading people list!")
                    }

                    override fun onSubscribe(d: Disposable?) {
                        compositeDesposable.add(d)
                    }
                })
    }



    private fun changePeopleDataSet(peopleList: List<People>) {
        this.peopleList = peopleList
        setChanged()
        notifyObservers()
    }



}