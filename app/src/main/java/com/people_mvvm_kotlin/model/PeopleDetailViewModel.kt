package com.people_mvvm_kotlin.model

import android.content.Context


class PeopleDetailViewModel {
    private var people: People? = null

    private lateinit var context: Context

    constructor(context: Context, people: People) {
        this.people = people
        this.context = context
    }

    fun getName():String? {
        return people?.name?.last
    }

    fun getLastName():String?{
        return people?.fullName
    }
}

